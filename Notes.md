In here I keep some notes about the molecular dynamics simulations I perform. For guidance on how to run any of the experiments, consult the main.rs function - essentially I parse an integer argument and map it to a specific experiment. To get the rest energy for example, do
```
cargo run -- 1
```
I also tried paralellizing the main for loops in the simulation code, on my machine (Lenovo thinkpad 15 inch with Ryzen 4700U 8 core CPU) I got roughly a 2x speedup.

## 1  Verify energy function, reproduce known results
Based on the data from the tables, aluminum has a lattice coefficient of roughly 4.0248 (65^1/3) at
zero kelvin. This is because this corresponds to the minimum of this plot:

![](molecularDynamicsSim/images/restEnergy.png)

obtained by running the function task_one in main.rs. Here we use a static model and some tables of known values for AL atom pair potentials, electron densities of AL pairs as a funcition of separation and electron embedding energies. The contributions to the energy are
pair interactions at a distance below 6.06 Å as well as a contribution from the embedding energy of the resulting electron desnity distribution. 

## 2 Excite aluminum, investigate nunerical stability of velocity Verlet algorithm
We can excite the aluminium lattice and simulate the resulting behaviour using the same tables as before together with the velocity Verlet algorithm. The algorithm essentially has us repeat the following logic each timestep
```
    velocity = velocity + 0.5 * acceleration * timestep
    position = position + velcoity * timestep
    acceleration = force(position) / atom_mass * timestep
    velocity = velocity + 0.5 * acceleration * timestep
```
We initiate the algorithm with the velocity and acceleration set to zero. For the positions, we need to create an excited initial state, in this example by moving all of the aluminium atoms a uniformly random distance distributed between +-6.5% of the lattice constant. This inital state (with no initial velocities or accelerations) is quite unnatural, so the metal will first relax into an equillibrium state, and then remain there for as long as the algorithm is numerically stable.

The 6.5% uniform perturbation of the atom positions has the same at a temperature of roughlt 750K.

Here is an example of the first 0.5 picosecond of a simulation with the above initial conditions, simulated with a timestep of 1 femtosecond. The code for these two plots is in the function `task_two`. 
![](molecularDynamicsSim/images/excitationEnergy.png)
The relaxation is clearly visible in the first 0.05 picoseconds. In this plot, the initial potential energy has been normalized to 0 because I thought that would be aesthetic. The equillibrum is stable for at least another 10000 timesteps, as can be seen in the following plot:
![](molecularDynamicsSim/images/excitationEnergyLong.png)
For the choice of the timestep, we have the following consideration to keep in mind:
 $v = \sqrt{2 E/m}$
 $E_{kin} \approx 0.1 eV$
 $M \approx 27/10000 \text{eV} \tfrac{\text{ps}^2}{\text{Å}^2}$
 $\Rightarrow v \approx 8 \tfrac{\text{Å}}{\text{ps}}$
Reasonably speaking, the simulation will turn bad when `velocity * timestep` approaches the lattice constant $4.0248$, i.e when the timestep becomes comparable to 0.5 picoseconds. We can investigate this in practice by way of simulation. Below are the total energies for a series of 11 simulations with different time ranging from 0.1 femtoseconds to 0.2 picoseconds.

![](molecularDynamicsSim/images/excitationPerTimestep.png)

Below ~10 femtoseconds (by rough ocular inspection), we have stable behaviour, with the total energy staying at 0 as would be expected (remember, in the previous plots we normalized the energy so that the potential started at 0, and the kinetic energy at 0).

Interestingly, we observe two failure modes here:
* Below the 0.2 picosecond timestep, the energy recieves a kick during the first few femtoseconds, before apparently stabilizing. The explanation is that
    * During the initial relaxation phase, we have larger velocities, so "Goodness bounds" are more easily violated. During this time, the numerical algorithm fails to closely approximate Hamiltonian mechanics.
    * After the relaxation phase, velocities seem to stablilize below the goodness bound, so energy doesn't keep being created.
* Above 0.2 picoseconds, the simulated sample of 256 aluminium atoms is but in a state from which it can not recover - most likely things like approximate periodicity and other invariants are irrepairably violated. In addition to this, atoms are able to travel almost an entire units cell per timestep, especially considering the extra numerical kick that has been given to their velocities.

Another note is that the kinetic energy of the system (not visible in the graph) as roughly constant for all of the samples up to and including the 0.161 ps timestep. This indicates that the failure of energy conservation is reflected more in the potential energy than the kinetic energy.

## 3 Equillibration of solid phase

Create function to prepare initial conditions corresponding to X temperature
task_three() asks for input - to decide whether to prepare initial conditions at temp T and/or pressure P. Then some more prompts for explicit values. If we want to start a simulation from equillibrated sample, we specify filename ofdesired initial condition. Similarly we will be prompted for a filename in which to save equillibrated initial conditions. 



## Ideas for future improvements

Velocity Verlet function could be augmented to take a function pointer to the `accel(positions)` subroutine and it would work on any system given mutable references to position, velocity, acceleration and the callback function.