pub mod molecular {
    use rayon::prelude::*;
    extern crate faster;
    use faster::*;

    /// Function that takes a matrix of size [4*N^3][3] and stores a face centered cube lattice in it. 
    /// N is the number of unit cells in each dimension and lattice_param is the lattice parameter. 
    /// 
    /// # Panics
    /// Panics if vec size is not equal to [4*N^3]
    /// Lattice param in units of Ångström
    pub fn init_fcc(positions: &mut Vec<[f64; 3]>, N: usize, lattice_param: f64)
    {
        assert_eq!(4*N*N*N, positions.len(), "Number of atoms (L) did not match the lattice size (R)!");
        let mut xor_value:usize;
        for i in 0..2*N {
            for j in 0..2*N {
                for k in 0..N{
                    if j%2 == i%2 {
                        xor_value = 0;
                    } else {
                        xor_value = 1;
                    }
                    positions[i * N * 2 * N + j * N + k][0] = lattice_param * (0.5 * xor_value as f64 + k as f64);
                    positions[i * N * 2 * N + j * N + k][1] = lattice_param * (j as f64 * 0.5);
                    positions[i * N * 2 * N + j * N + k][2] = lattice_param * (i as f64 * 0.5);
                }
            }
        }
    }

    // Below are some functions to  calculate properties (potential energy, forces, etc.) 
    // of a set of Aluminum atoms using an embedded atom model (EAM) potential.  
    // Note that the pair potential and electron density end right at rcut = 6.06
    // (see get_X_al functions for meaning of the cutoff distance rcut)
    const PAIR_POTENTIAL_ROWS: usize = 18;
    const PAIR_POTENTIAL: [f64; 90] = [2.0210, 2.2730, 2.4953, 2.7177, 2.9400, 3.1623, 3.3847, 3.6070,
                                    3.8293, 4.0517, 4.2740, 4.4963, 4.7187, 4.9410, 5.1633, 5.3857,
                                    5.6080, 6.0630, 2.0051, 0.7093, 0.2127, 0.0202, -0.0386, -0.0492,
                                    -0.0424, -0.0367, -0.0399, -0.0574, -0.0687, -0.0624, -0.0492,
                                    -0.0311, -0.0153, -0.0024, -0.0002, 0.0, -7.2241, -3.3383, -1.3713,
                                    -0.4753, -0.1171, 0.0069, 0.0374, 0.0122, -0.0524, -0.0818,
                                    -0.0090, 0.0499, 0.0735, 0.0788, 0.0686, 0.0339, -0.0012,
                                    0.0, 9.3666, 6.0533, 2.7940, 1.2357, 0.3757, 0.1818, -0.0445,
                                    -0.0690, -0.2217, 0.0895, 0.2381, 0.0266, 0.0797, -0.0557, 0.0097,
                                    -0.1660, 0.0083, 0.0, -4.3827, -4.8865, -2.3363, -1.2893, -0.2907,
                                    -0.3393, -0.0367, -0.2290, 0.4667, 0.2227, -0.3170, 0.0796,
                                    -0.2031, 0.0980, -0.2634, 0.2612, -0.0102, 0.0];

    const ELECTRON_DENSITY_ROWS: usize = 15;
    const ELECTRON_DENSITY: [f64; 75] = [2.0210, 2.2730, 2.5055, 2.7380, 2.9705, 3.2030, 3.4355, 3.6680,
                                        3.9005, 4.1330, 4.3655, 4.5980, 4.8305, 5.0630, 6.0630, 0.0824,
                                        0.0918, 0.0883, 0.0775, 0.0647, 0.0512, 0.0392, 0.0291, 0.0186,
                                        0.0082, 0.0044, 0.0034, 0.0027, 0.0025, 0.0000, 0.0707, 0.0071,
                                        -0.0344, -0.0533, -0.0578, -0.0560, -0.0465, -0.0428, -0.0486,
                                        -0.0318, -0.0069, -0.0035, -0.0016, -0.0008, 0.0, -0.1471, -0.1053,
                                        -0.0732, -0.0081, -0.0112, 0.0189, 0.0217, -0.0056, -0.0194,
                                        0.0917, 0.0157, -0.0012, 0.0093, -0.0059, 0.0, 0.0554, 0.0460,
                                        0.0932, -0.0044, 0.0432, 0.0040, -0.0392, -0.0198, 0.1593,
                                        -0.1089, -0.0242, 0.0150, -0.0218, 0.0042, 0.0];

    const EMBEDDING_ENERGY_ROWS: usize = 13;
    const EMBEDDING_ENERGY: [f64; 65] = [0.0, 0.1000, 0.2000, 0.3000, 0.4000, 0.5000, 0.6000, 0.7000,
                                        0.8000, 0.9000, 1.0000, 1.1000, 1.2000, 0.0, -1.1199, -1.4075,
                                        -1.7100, -1.9871, -2.2318, -2.4038, -2.5538, -2.6224, -2.6570,
                                        -2.6696, -2.6589, -2.6358, -18.4387, -5.3706, -2.3045, -3.1161,
                                        -2.6175, -2.0666, -1.6167, -1.1280, -0.4304, -0.2464, -0.0001,
                                        0.1898, 0.2557, 86.5178, 44.1632, -13.5018, 5.3853, -0.3996,
                                        5.9090, -1.4103, 6.2976, 0.6785, 1.1611, 1.3022, 0.5971, 0.0612,
                                        -141.1819, -192.2166, 62.9570, -19.2831, 21.0288, -24.3978,
                                        25.6930, -18.7304, 1.6087, 0.4704, -2.3503, -1.7862, -1.7862];

    /// Evaluates the spline in the variable x. 
    /// Piecewise 3rd order is what spline means https://en.wikipedia.org/wiki/Spline_interpolation
    /// The table argument is one of the const tables from above. TODO: Can I express this in code?
    /// m is the number of rows (or columns?? In the table.)
    /// x is expected to be in [0, 14]
    /// There are always 5 columns, the first one is used to look for x, the other 4 to somehow define
    /// the spline polynomial
    fn spline_eval(x: f64, table: Vec<f64>, m: usize) -> f64{
        let mut k_low = 0;
        let mut k_high = m;
        let mut result = 0.0;
        let mut k: usize;

        // Find closest table[k] to x by binary search
        while(k_high - k_low > 1){
            k = (k_high + k_low) >> 1;
            if table[k] > x{
                k_high = k;
            } else {
                k_low = k;
            }
        }

        // Go to local coordinates
        let x = x - table[k_low];

        result = table[(k_low + 4*m)];
        /* Apply Horner's Scheme */
        // I feel like this is clearly using something about table that I don't know. 
        // Maybe refer the wiki for Horner's method and I'll be able to figure it out.
        for i in (1..4).rev() {
            result *= x;
            result += table[(k_low + i*m)];
        }
        return result;
    }

    ///Evaluates the derivative of the spline in x
    fn spline_eval_diff(x: f64, table: Vec<f64>, m: usize) -> f64{
        let mut k_low = 0;
        let mut k_high = m;
        let mut result = 0.0;
        let mut k: usize;

        // Find closest table[k] to x by binary search
        while(k_high - k_low > 1){
            k = (k_high + k_low) >> 1;
            if table[k] > x{
                k_high = k;
            } else {
                k_low = k;
            }
        }

        let x = x - table[k_low];

        result = 3.0 * table[k_low + 4*m];
        /* Apply Horner's Scheme */
        // To find the derivative, we are multiplying the result by three and doing two iterations
        // insted of three, so previous thing was a 3rd order polynomial fit and this is it's derivative
        // A spline is a third order poly between each point in dataset
        for i in (2..4).rev() {
            result *= x;
            result += ((i-1) as f64)*table[k_low + i*m];
        }
        result
    }

    // Uses the positions of the atoms in the lattice, together with embedding energy
    // single molecule electron density tables to calculate the total electron density and
    // from that resulting forces on the lattice points.
    pub fn get_forces_AL(forces: &mut Vec<[f64; 3]>, positions: &Vec<[f64; 3]>,
                    cell_length: f64, nbr_atoms: usize){
        let rcut = 6.06;
        let rcut_squared = rcut * rcut;

        let inverse_cell_length = 1.0 / cell_length;
        let cell_length_squared = cell_length * cell_length;

        // Forces on each atom
        let mut fx = vec![0.0; nbr_atoms];
        let mut fy = vec![0.0; nbr_atoms];
        let mut fz = vec![0.0; nbr_atoms];

        let mut density = vec![0.0; nbr_atoms];
        let mut dUembed_drho = vec![0.0; nbr_atoms];
        
        // Positions in units normalized to cell length
        let mut sx = vec![0.0; nbr_atoms];
        let mut sy = vec![0.0; nbr_atoms];
        let mut sz = vec![0.0; nbr_atoms];

        for i in 0..nbr_atoms{
            sx[i] = positions[i][0] * inverse_cell_length;
            sy[i] = positions[i][1] * inverse_cell_length;
            sz[i] = positions[i][2] * inverse_cell_length;
        }

        // Loop to compute densities 
        for i in 0..nbr_atoms {
            // Translate current particle coord to positive quadrant of unit k-space cell
            let sxi = sx[i] - sx[i].floor();
            let syi = sy[i] - sy[i].floor();
            let szi = sz[i] - sz[i].floor();

            // Loop over other atoms to compute density contributions from i-j pairs (paralellized)
            let densityi: f64 = (i+1..nbr_atoms).into_par_iter().zip(density[i+1..nbr_atoms].par_iter_mut())
                .map(|(j, densityj)| {
                    // Periodically translate atom j to positive quadrant and then calculate distance
                    let mut sxij = sxi - (sx[j] - sx[j].floor());
                    let mut syij = syi - (sy[j] - sy[j].floor());
                    let mut szij = szi - (sz[j] - sz[j].floor());

                    // Periodic boundary conditions:
                    sxij = sxij - (sxij + 0.5).floor();
                    syij = syij - (syij + 0.5).floor();
                    szij = szij - (szij + 0.5).floor();

                    //Squared i-j distance squared
                    let rij_squared = cell_length_squared * (sxij*sxij + syij*syij + szij*szij);

                    //Add force and energy contribution if distance is smaller than rcut
                    if(rij_squared < rcut_squared){
                        let rij = rij_squared.sqrt();
                        let dens = spline_eval(rij, ELECTRON_DENSITY.to_vec(),
                                        ELECTRON_DENSITY_ROWS);
                        *densityj += dens;
                        dens
                    } else {
                        0.0
                    }
                })
                .sum();
            density[i] += densityi;
        }

        // Loop to calculate derivative of embedding function
        for i in 0..nbr_atoms {
            dUembed_drho[i] = spline_eval_diff(density[i], EMBEDDING_ENERGY.to_vec(),
                            EMBEDDING_ENERGY_ROWS);
        }

        // Loop to compute forces 
        for i in 0..nbr_atoms {
            // Translate current particle coord to positive quadrant of unit k-space cell
            let sxi = sx[i] - sx[i].floor();
            let syi = sy[i] - sy[i].floor();
            let szi = sz[i] - sz[i].floor();

            let (fxi, fyi, fzi) = (i+1..nbr_atoms).into_par_iter().zip((fx[i+1..nbr_atoms].par_iter_mut(),
                                                                        fy[i+1..nbr_atoms].par_iter_mut(),
                                                                        fz[i+1..nbr_atoms].par_iter_mut()))
                .map(|(j, (fxj, fyj, fzj))| {
                    // Periodically translate atom j to positive quadrant and then calculate distance
                    let mut sxij = sxi - (sx[j] - sx[j].floor());
                    let mut syij = syi - (sy[j] - sy[j].floor());
                    let mut szij = szi - (sz[j] - sz[j].floor());

                    // Periodic boundary conditions:
                    sxij = sxij - (sxij + 0.5).floor();
                    syij = syij - (syij + 0.5).floor();
                    szij = szij - (szij + 0.5).floor();

                    //Squared i-j distance squared
                    let rij_squared = cell_length_squared * (sxij*sxij + syij*syij + szij*szij);

                    //Add force and energy contribution if distance is smaller than rcut
                    if(rij_squared < rcut_squared){
                        let rij = rij_squared.sqrt();
                        let dUpair_dr = spline_eval_diff(rij, PAIR_POTENTIAL.to_vec(),
                                                            PAIR_POTENTIAL_ROWS);
                        let drho_dr = spline_eval_diff(rij, ELECTRON_DENSITY.to_vec(),
                                                            ELECTRON_DENSITY_ROWS);
                        
                        // Add force contribution from i-j interaction
                        let force = (dUpair_dr + (dUembed_drho[i] + dUembed_drho[j])*drho_dr) / rij;
                        let fxi = - force * sxij * cell_length;
                        let fyi = - force * syij * cell_length;
                        let fzi = - force * szij * cell_length;
                        *fxj += force * sxij * cell_length;
                        *fyj += force * syij * cell_length;
                        *fzj += force * szij * cell_length;
                        (fxi, fyi, fzi)
                    } else {
                        (0.0, 0.0, 0.0)
                    }
                })
                .reduce(|| (0.0, 0.0, 0.0), |(acc_x, acc_y, acc_z), (x, y, z)| {
                    (acc_x + x, acc_y + y, acc_z + z)
            });
            fx[i] += fxi;
            fy[i] += fyi;
            fz[i] += fzi;          
        }

        for i in 0..nbr_atoms {
            forces[i][0] = fx[i];
            forces[i][1] = fy[i];
            forces[i][2] = fz[i];
        }
    }

    /// returns the potential energy of the entire system
    /// in units of eV
    /// cell_length is the 1D size of the sample - N * lattice_parameter
    pub fn get_energy_AL(positions: &Vec<[f64; 3]>, cell_length: f64, nbr_atoms: usize) -> f64{
        let rcut = 6.06;
        let rcut_squared = rcut * rcut;

        let inverse_cell_length = 1.0 / cell_length;
        let cell_length_squared = cell_length * cell_length;

        let mut density = vec![0.0; nbr_atoms];
        let mut energy = 0.0;
        
        // Positions in units normalized to cell length
        let mut sx = vec![0.0; nbr_atoms];
        let mut sy = vec![0.0; nbr_atoms];
        let mut sz = vec![0.0; nbr_atoms];
        for i in 0..nbr_atoms{
            sx[i] = positions[i][0] * inverse_cell_length;
            sy[i] = positions[i][1] * inverse_cell_length;
            sz[i] = positions[i][2] * inverse_cell_length;
        }

        // Loop to compute densities 
        for i in 0..nbr_atoms {
            // Translate current particle coord to positive quadrant of unit k-space cell
            let sxi = sx[i] - sx[i].floor();
            let syi = sy[i] - sy[i].floor();
            let szi = sz[i] - sz[i].floor();

            // TODO: There is code reuse for this loop (four times!!!). It should be possible to make this a modular function.
            // Loop over other atoms to compute density contributions from i-j pairs (paralellized)
            let (densityi, energy_i): (f64, f64) = (i+1..nbr_atoms).into_par_iter().zip(density[i+1..nbr_atoms].par_iter_mut())
                .map(|(j, densityj)| {
                    // Periodically translate atom j to positive quadrant and then calculate distance
                    let mut sxij = sxi - (sx[j] - sx[j].floor());
                    let mut syij = syi - (sy[j] - sy[j].floor());
                    let mut szij = szi - (sz[j] - sz[j].floor());

                    // Periodic boundary conditions:
                    sxij = sxij - (sxij + 0.5).floor();
                    syij = syij - (syij + 0.5).floor();
                    szij = szij - (szij + 0.5).floor();

                    //Squared i-j distance squared
                    let rij_squared = cell_length_squared * (sxij*sxij + syij*syij + szij*szij);

                    //Add force and energy contribution if distance is smaller than rcut
                    if(rij_squared < rcut_squared){
                        let rij = rij_squared.sqrt();
                        let dens = spline_eval(rij, ELECTRON_DENSITY.to_vec(),
                                        ELECTRON_DENSITY_ROWS);
                        *densityj += dens;
                        let energy_ij = spline_eval(rij, PAIR_POTENTIAL.to_vec(), PAIR_POTENTIAL_ROWS);
                        (dens, energy_ij)
                    } else {
                        (0.0, 0.0)
                    }
                })
                .reduce(|| (0.0, 0.0), |(a1, a2), (v1, v2)| (a1 + v1, a2+ v2));
            energy += energy_i;
            density[i] += densityi;
        }

        // Loop to add embedding energy contribution
        for i in 0..nbr_atoms {
            energy += spline_eval(density[i], EMBEDDING_ENERGY.to_vec(),
                                  EMBEDDING_ENERGY_ROWS)
        }
        energy
    }


    pub fn get_virial_AL(positions: &Vec<[f64; 3]>, cell_length: f64, nbr_atoms: usize) -> f64{
        let rcut = 6.06;
        let rcut_squared = rcut * rcut;

        let inverse_cell_length = 1.0 / cell_length;
        let cell_length_squared = cell_length * cell_length;

        let mut density = vec![0.0; nbr_atoms];
        let mut dUembed_drho = vec![0.0; nbr_atoms];
        
        // Positions in units normalized to cell length
        let mut sx = vec![0.0; nbr_atoms];
        let mut sy = vec![0.0; nbr_atoms];
        let mut sz = vec![0.0; nbr_atoms];
        for i in 0..nbr_atoms{
            sx[i] = positions[i][0] * inverse_cell_length;
            sy[i] = positions[i][1] * inverse_cell_length;
            sz[i] = positions[i][2] * inverse_cell_length;
        }

        // Loop to compute densities 
        for i in 0..nbr_atoms {
            // Translate current particle coord to positive quadrant of unit k-space cell
            let sxi = sx[i] - sx[i].floor();
            let syi = sy[i] - sy[i].floor();
            let szi = sz[i] - sz[i].floor();
            let mut densityi = density[i];

             // Loop over other atoms to compute density contributions from i-j pairs (paralellized)
             let densityi: f64 = (i+1..nbr_atoms).into_par_iter().zip(density[i+1..nbr_atoms].par_iter_mut())
                .map(|(j, densityj)| {
                    // Periodically translate atom j to positive quadrant and then calculate distance
                    let mut sxij = sxi - (sx[j] - sx[j].floor());
                    let mut syij = syi - (sy[j] - sy[j].floor());
                    let mut szij = szi - (sz[j] - sz[j].floor());

                    // Periodic boundary conditions:
                    sxij = sxij - (sxij + 0.5).floor();
                    syij = syij - (syij + 0.5).floor();
                    szij = szij - (szij + 0.5).floor();

                    //Squared i-j distance squared
                    let rij_squared = cell_length_squared * (sxij*sxij + syij*syij + szij*szij);

                    //Add force and energy contribution if distance is smaller than rcut
                    if(rij_squared < rcut_squared){
                        let rij = rij_squared.sqrt();
                        let dens = spline_eval(rij, ELECTRON_DENSITY.to_vec(),
                                        ELECTRON_DENSITY_ROWS);
                        *densityj += dens;
                        dens
                    } else {
                        0.0
                    }
                })
                .sum();
             density[i] += densityi;
        }

        // Loop to calculate derivative of embedding function
        for i in 0..nbr_atoms {
            dUembed_drho[i] = spline_eval_diff(density[i], EMBEDDING_ENERGY.to_vec(),
            EMBEDDING_ENERGY_ROWS);
        }

        let mut virial = 0.0;

        // Loop over pairs again 
        for i in 0..nbr_atoms {
            // Translate current particle coord to positive quadrant of unit k-space cell
            let sxi = sx[i] - sx[i].floor();
            let syi = sy[i] - sy[i].floor();
            let szi = sz[i] - sz[i].floor();

            let add_to_virial: f64 = (i+1..nbr_atoms).into_par_iter().zip(density[i+1..nbr_atoms].par_iter_mut())
             .map(|(j, densityj)| {
                 // Periodically translate atom j to positive quadrant and then calculate distance
                 let mut sxij = sxi - (sx[j] - sx[j].floor());
                 let mut syij = syi - (sy[j] - sy[j].floor());
                 let mut szij = szi - (sz[j] - sz[j].floor());

                 // Periodic boundary conditions:
                 sxij = sxij - (sxij + 0.5).floor();
                 syij = syij - (syij + 0.5).floor();
                 szij = szij - (szij + 0.5).floor();

                 //Squared i-j distance squared
                 let rij_squared = cell_length_squared * (sxij*sxij + syij*syij + szij*szij);

                 //Add force and energy contribution if distance is smaller than rcut
                if(rij_squared < rcut_squared){
                    let rij = rij_squared.sqrt();
                    let dUpair_dr = spline_eval_diff(rij, PAIR_POTENTIAL.to_vec(),
                                                        PAIR_POTENTIAL_ROWS);
                    let drho_dr = spline_eval_diff(rij, ELECTRON_DENSITY.to_vec(),
                                                        ELECTRON_DENSITY_ROWS);

                    let force = -(dUpair_dr + (dUembed_drho[i] + dUembed_drho[j])*drho_dr) / rij;
                    let virial = force * rij_squared;
                    virial
                } else {
                    0.0
                }
             })
             .sum();
             virial += add_to_virial;
        }
        virial / 3.0
    }

    /// Prototype functions for attemt at simd using the faster crate below
    /// completely forgot to parallelize them, so didn't improve perf

    // n is a standin for x, y or z in all of the variable names
    // makes an array of relative distances between ith and jth particles in lattice
    // and imposes periodicity and boundary conditions through the two simd_map()s
    fn simd_make_snijs(sni: f64, sn: &Vec<f64>) -> Vec<f64>{
        sn.simd_iter(f64s(0.0))
                    .simd_map(|snj| sni - (snj - snj.floor()))
                    .simd_map(|snij| snij - (snij + f64s(0.5)).floor())
                    .scalar_collect()
    }

    // returns a vector of rij and rij^2 (with rij being the destance between particle i and j)
    fn simd_make_rijs(sxi: f64, syi: f64, szi: f64,
            sx: &Vec<f64>, sy: &Vec<f64>, sz: &Vec<f64>,
            cell_length_squared: f64) -> (Vec<f64>, Vec<f64>){
        
        let sxijs = simd_make_snijs(sxi, sx);
        let syijs = simd_make_snijs(syi, sy);
        let szijs = simd_make_snijs(szi, sz);
        let rijs_sq = 
        (sxijs.simd_iter(f64s(0.0)), syijs.simd_iter(f64s(0.0)), szijs.simd_iter(f64s(0.0)))
            .zip()
            .simd_map(
                |(sxij, syij, szij)| {
                (sxij*sxij + syij*syij + szij*szij) * cell_length_squared
            })
            .scalar_collect();
        let rijs = rijs_sq.simd_iter(f64s(0.0))
                      .simd_map(|r| r.sqrt())
                      .scalar_collect();
        (rijs_sq, rijs)
    }

    // High flying ambition - interface with the C functions this is supposed to replace
    // and check that they act the same. Maybe don't have to since I get physically reasonable results now
    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn makes_one_fcc_cell(){//Add TC that panics...
            let mut positions = vec![[0.0; 3]; 4];

            init_fcc(&mut positions, 1, 1.0);

            let expected_positions = vec![[0.0, 0.0, 0.0],
                                                        [0.5, 0.5, 0.0],
                                                        [0.5, 0.0, 0.5],
                                                        [0.0, 0.5, 0.5]];
            assert_eq!(expected_positions, positions);
        }

        //TODO: Test spline derivative? It will trivially pass equivalents of the below normal spline
        // tests, so I don't know that I can be bothered.
        #[test]
        fn spline_eval_simple_example(){
            let value = spline_eval(0.75, EMBEDDING_ENERGY.to_vec(),
                                        EMBEDDING_ENERGY_ROWS);
            let target = 0.5671;
            assert!((value - target).abs() < 0.01, "Float value was {}, expected {}", value, target);
        }
        #[test]
        fn spline_eval_embedding_energy_two_calls_same_result(){
            let value = spline_eval(1.4, EMBEDDING_ENERGY.to_vec(),
                                        EMBEDDING_ENERGY_ROWS);
            let target = spline_eval(2.0, EMBEDDING_ENERGY.to_vec(),
            EMBEDDING_ENERGY_ROWS);
            assert!((value - target).abs() < 0.01, "Float value was {}, expected {}", value, target);
        }
        #[test]
        fn spline_eval_pair_potential_two_calls_same_result(){
            let value = spline_eval(6.5, PAIR_POTENTIAL.to_vec(),
                                        PAIR_POTENTIAL_ROWS);
            let target = spline_eval(7.6, PAIR_POTENTIAL.to_vec(),
            PAIR_POTENTIAL_ROWS);
            assert!((value - target).abs() < 0.01, "Float value was {}, expected {}", value, target);
        }
        #[test]
        fn spline_eval_electron_density_two_calls_same_result(){
            let value = spline_eval(-0.5, ELECTRON_DENSITY.to_vec(),
                                        ELECTRON_DENSITY_ROWS);
            let target = spline_eval(-2.0, ELECTRON_DENSITY.to_vec(),
            ELECTRON_DENSITY_ROWS);
            assert!((value - target).abs() < 0.01, "Float value was {}, expected {}", value, target);
        }
    }

}