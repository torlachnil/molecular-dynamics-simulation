use molecular_dynamics_sim::molecular::*;
extern crate plotters;
use plotters::prelude::*;
use std::time::{Duration, Instant};
use std::io;
use std::io::*;

use std::env::args;
use rand::Rng;

const ALUMINIUM_MASS: f64 = 26.982 / 9649.0; //26.982 a. u. in units of eV - au to ev conversion 27.211
// TODO: correct this mass - unit has to be in eV(picosecond^2)/Å^2. The unit should be 9649 times the atomic mass unit
// That means that mass in terms of atomic mass units should be divided by 9649 to be in the new mass units.

fn main() {
    let task: u32 = args().nth(1).expect("Expected an argument telling me what task to execute!")
                        .parse::<u32>().unwrap();
    // Take commandline arguments, based on arg from 1-max no of tasks, run that task.
    let now = Instant::now();
    match task {
        1 => task_one(),
        2 => task_two(),
        2001 => task_two_timestep_dependence(),
        3001 => task_three_prep_boundary_conditions(),
        _ => println!("Invalid task specified!"),
    }
    println!("{}", now.elapsed().as_millis());
}

fn task_one(){
    const N: usize = 10;
    let nbr_atoms = 4*N*N*N;
    let mut positions = vec![[0.0; 3]; nbr_atoms];
    let mut a0;

    const number_of_samples: usize = 100;
    let mut energies = [0.0; number_of_samples];
    let mut volumes = [0.0; number_of_samples];
    let mut E_pot: f64;

    for i in 0..number_of_samples{
        a0 = 3.9 + i as f64 / (number_of_samples as f64 / 0.35);
        init_fcc(&mut positions, N, a0);

        E_pot = get_energy_AL(&positions, a0 * (N as f64), nbr_atoms);
        energies[i] = E_pot / (N*N*N) as f64;// E per unit cell
        volumes[i] = a0*a0*a0;
        println!("Energy {} is {}, lattice constant is {}", i, energies[i], a0);
    }
    
    println!("Done with data, now plotting!");
    
    let root_area = BitMapBackend::new("images/restEnergy.png", (500, 400))
                    .into_drawing_area();
    root_area.fill(&WHITE).unwrap();
    
    let (max_volume, min_volume) = maxmin(&volumes.to_vec());
    let (max_energy, min_energy) = maxmin(&energies.to_vec());
    let mut chart = ChartBuilder::on(&root_area)
        .set_label_area_size(LabelAreaPosition::Left, 60)
        .set_label_area_size(LabelAreaPosition::Bottom, 40)
        .caption("Energy per unit cell (AL)", ("Arial", 40))
        .build_ranged(min_volume..max_volume, min_energy..max_energy)
        .unwrap();
    

    chart.configure_mesh()
        .x_desc("Unit cell volume [Å^3]")
        .y_desc("Energy [eV]")
        .draw().unwrap();
    chart.draw_series(
        LineSeries::new((0..volumes.len()).map(|i| (volumes[i], energies[i]) ), &GREEN)
    ).unwrap();
}


fn task_two(){
    const N: usize = 4;

    const dt: f64 = 0.001;
    let a0 = 4.0248;
    let nbr_atoms = 4*N*N*N;
    let mut positions = vec![[0.0; 3]; nbr_atoms];
    let mut velocities = vec![[0.0; 3]; nbr_atoms];
    let mut accelerations = vec![[0.0; 3]; nbr_atoms];
    let mut forces = vec![[0.0; 3]; nbr_atoms];
    init_fcc(&mut positions, N, a0);
    // Introduce small random deviations from the initial lattice positions. Uniform
    // deviations in the interval +- 6.5% of a0.
    perturb_lattice(&mut positions, a0);
    let initial_energy = get_energy_AL(&positions, a0*N as f64, nbr_atoms);

    let mut potential_energies: Vec<f64> = Vec::new();
    let mut kinetic_energies: Vec<f64> = Vec::new();
    //const RELAXATION_TIMESTEPS: usize = 10;
    const NO_OF_TIMESTEPS: usize = 500;
    let mut times = vec![0.0; NO_OF_TIMESTEPS];
    for i in 0..NO_OF_TIMESTEPS{
        times[i] = i as f64 * dt;
    }
    
    for _ in 0..NO_OF_TIMESTEPS{
        velocity_verlet_timestep(&mut positions, &mut velocities, &mut accelerations, &mut forces,
                                 a0, dt, N, nbr_atoms);
        let mut kinetic_energy = 0.0;
        for v in velocities.iter(){
            kinetic_energy += (v[0]*v[0] + v[1]*v[1] + v[2]*v[2]) / 2.0 * ALUMINIUM_MASS;
        }
        potential_energies.push( (get_energy_AL(&positions, a0*N as f64, nbr_atoms)
                                - initial_energy) / nbr_atoms as f64);
        kinetic_energies.push(kinetic_energy / nbr_atoms as f64);
    }

    // TODO: make function for eV->T conversion, make function for average of vec float
    println!("The average temperature is {}K", 2.0/3.0*1.16*10000.0*kinetic_energies.iter().sum::<f64>()/(kinetic_energies.len() as f64));
  
    // Plot kinetic, potential, total energy
    // Compute the average temperature (and see that it is around 600-800K).
    let (maxtime, mintime) = maxmin(&times);
    let (maxpotential, minpotential) = maxmin(&potential_energies);
    let (maxkinetic, minkinetic) = maxmin(&kinetic_energies);
    let (maxsum, minsum) = maxmin(
        &potential_energies.iter().zip(kinetic_energies.iter()).map(|(x,y)| x + y)
        .collect()
    );
    let (maxenergy, minenergy) = maxmin(
        &vec![maxsum, minsum, maxpotential, minpotential, maxkinetic, minkinetic]);
    


    let root_area = BitMapBackend::new("images/excitationEnergy.png", (500, 400))
                .into_drawing_area();
    root_area.fill(&WHITE).unwrap();
    let mut chart = ChartBuilder::on(&root_area)
        .set_label_area_size(LabelAreaPosition::Left, 60)
        .set_label_area_size(LabelAreaPosition::Bottom, 40)
        .caption("Energy per atom (AL)", ("Arial", 40))
        .build_ranged(mintime..maxtime, minenergy..maxenergy)
        .unwrap();

    chart.configure_mesh()
        .x_desc("Time [picoseconds]")
        .y_desc("Energy [eV]")
        .draw().unwrap();

    chart.draw_series(
        LineSeries::new((0..times.len()).map(|i| (times[i], kinetic_energies[i]) ), &GREEN)
    ).unwrap()
    .label("Kinetic energy")
    .legend(|(x, y)| Path::new(vec![(x, y), (x + 20, y)], &GREEN));
    chart.draw_series(
        LineSeries::new((0..times.len()).map(|i| (times[i], potential_energies[i]) ), &BLUE)
    ).unwrap()
    .label("Potential energy")
    .legend(|(x, y)| Path::new(vec![(x, y), (x + 20, y)], &BLUE));
    chart.draw_series(
        LineSeries::new((0..times.len()).map(|i| (times[i], potential_energies[i] + kinetic_energies[i]) )
        , &RED)
    ).unwrap()
    .label("Total energy")
    .legend(|(x, y)| Path::new(vec![(x, y), (x + 20, y)], &RED));

    chart.configure_series_labels()
        .border_style(&BLACK)
        .background_style(&WHITE.mix(0.8))
        .draw()
        .unwrap();
    // How is energy conservation related to the size of the timestep?
}



fn perturb_lattice(positions: &mut Vec<[f64; 3]>, a0: f64){
    let mut rng = rand::thread_rng();
    let mut perturbation;
    for position in positions.iter_mut(){
        for coordinate in position.iter_mut(){
            perturbation = a0 * rng.gen_range(-0.065, 0.065);
            *coordinate = *coordinate + perturbation;
        }
    }
}

fn velocity_verlet_timestep(positions: &mut Vec<[f64; 3]>, velocities: &mut Vec<[f64; 3]>,
                            accelerations: &mut Vec<[f64; 3]>,
                            mut forces: &mut Vec<[f64; 3]>, a0: f64, dt: f64, N: usize, nbr_atoms: usize) {
    // Solve equations of motion using the velocity Verlet algorithm. Implement periodic boundary
    // conditions in the same way as in the help code (get_energy_AL and friends).
    for ((position, velocity), acceleration) 
        in positions.iter_mut().zip(velocities.iter_mut()).zip(accelerations.iter()){
        for i in 0..3 {
            velocity[i] += 0.5 * acceleration[i] * dt;
            position[i] += velocity[i] * dt;
        }
    }
    get_forces_AL(&mut forces, &positions, a0 * N as f64, nbr_atoms);
    for (force, acceleration) in forces.iter().zip(accelerations.iter_mut()){
        for i in 0..3{
            acceleration[i] = force[i] / ALUMINIUM_MASS;
        }
    }
    for (velocity, acceleration) in
         velocities.iter_mut().zip(accelerations.iter()){
        for i in 0..3 {
            velocity[i] += 0.5 * acceleration[i] * dt;
        }
    }
}

// No idea how this handles f64 NaN, I don't intend to care.
fn maxmin(input: &Vec<f64>) -> (f64, f64){
    let mut max = std::f64::NEG_INFINITY;
    let mut min = std::f64::INFINITY;
    for element in input{
        if max < *element {
            max = *element;
        }
        if min > *element {
            min = *element;
        }
    }
    (max, min)
}


fn task_two_timestep_dependence(){
    const N: usize = 4;
    let a0 = 4.0248;
    let nbr_atoms = 4*N*N*N;
    let mut positions = vec![[0.0; 3]; nbr_atoms];
    let mut velocities = vec![[0.0; 3]; nbr_atoms];
    let mut accelerations = vec![[0.0; 3]; nbr_atoms];
    let mut forces = vec![[0.0; 3]; nbr_atoms];
    let root_area = BitMapBackend::new("images/excitationPerTimestep.png", (500, 400))
                    .into_drawing_area();
    root_area.fill(&WHITE).unwrap();

    // Defining dts before chart so that it is dropped later
    const SPAN: f64 = 0.022;
    let dts = (0..11).map(|i| (0.0001 + SPAN/11.0*i as f64) as f64)
                               .collect::<Vec<f64>>();

    let mut chart = ChartBuilder::on(&root_area)
        .set_label_area_size(LabelAreaPosition::Left, 60)
        .set_label_area_size(LabelAreaPosition::Bottom, 40)
        .caption("Energy per atom (AL)", ("Arial", 40))
        .build_ranged(0.0..0.5, 0.0..1.0)
        .unwrap();

    chart.configure_mesh()
        .x_desc("Time [picoseconds]")
        .y_desc("Energy [eV]")
        .draw().unwrap();
    
    for dt in dts.iter(){
        init_fcc(&mut positions, N, a0);
        velocities = vec![[0.0; 3]; nbr_atoms];
        accelerations = vec![[0.0; 3]; nbr_atoms];
        forces = vec![[0.0; 3]; nbr_atoms];    
        // Introduce small random deviations from the initial lattice positions. Uniform
        // deviations in the interval +- 6.5% of a0.
        perturb_lattice(&mut positions, a0);
        let initial_energy = get_energy_AL(&positions, a0*N as f64, nbr_atoms);

        let mut potential_energies: Vec<f64> = Vec::new();
        let mut kinetic_energies: Vec<f64> = Vec::new();
        //const RELAXATION_TIMESTEPS: usize = 10;
        let NO_OF_TIMESTEPS: usize = (2.0/dt) as usize;
        let mut times = vec![0.0; NO_OF_TIMESTEPS];
        for i in 0..NO_OF_TIMESTEPS{
            times[i] = i as f64 * dt;
        }

        for _ in 0..NO_OF_TIMESTEPS{
            velocity_verlet_timestep(&mut positions, &mut velocities, &mut accelerations, &mut forces,
                                    a0, *dt, N, nbr_atoms);
            let mut energy = 0.0;
            for v in velocities.iter(){
                energy += (v[0]*v[0] + v[1]*v[1] + v[2]*v[2]) / 2.0 * ALUMINIUM_MASS;
            }
            potential_energies.push( (get_energy_AL(&positions, a0*N as f64, nbr_atoms)
                                    - initial_energy) / nbr_atoms as f64);
            kinetic_energies.push(energy / nbr_atoms as f64);
        }

        // TODO: make function for eV->T conversion, make function for average of vec float
        println!("The average temperature is {}K", 2.0/3.0*1.16*10000.0*kinetic_energies.iter().sum::<f64>()/(kinetic_energies.len() as f64));

        // Plot kinetic, potential, total energy
        // Compute the average temperature (and see that it is around 600-800K).
        let (maxtime, mintime) = maxmin(&times);
        let (maxpotential, minpotential) = maxmin(&potential_energies);
        let (maxkinetic, minkinetic) = maxmin(&kinetic_energies);
        let (maxsum, minsum) = maxmin(
            &potential_energies.iter().zip(kinetic_energies.iter()).map(|(x,y)| x + y)
            .collect()
        );
        let (maxenergy, minenergy) = maxmin(
            &vec![maxsum, minsum, maxpotential, minpotential, maxkinetic, minkinetic]);

        chart.draw_series(
            LineSeries::new((0..times.len()).map(|i| (times[i], potential_energies[i] + kinetic_energies[i]) )
            , &RGBColor(255 - (255.0 * dt/SPAN) as u8, 0, (255.0 * dt/SPAN) as u8))
            ).unwrap()
            .label(format!("Total energy with dt {}", dt))
            .legend(move |(x, y)| Path::new(vec![(x, y), (x + 20, y)],
                &RGBColor(255 - (255.0 * dt/SPAN) as u8, 0, (255.0 * dt/SPAN) as u8))
        );

        
    }
    chart.configure_series_labels()
            .border_style(&BLACK)
            .background_style(&WHITE.mix(0.8))
            .draw()
            .unwrap();
}



fn task_three_prep_boundary_conditions(){
    let mut input = String::new();
    println!("Please choose target temperature in K (float or int)");
    io::stdin().read_line(&mut input).expect("error: unable to read user input");
    trim_newline(&mut input);
    let temperature = input.parse::<f64>().expect(&format!("Given temperature ({}) was not a valid float!", input)[..]);
    input = String::new();
    println!("Please choose target pressure in bars (float or int)");
    io::stdin().read_line(&mut input).expect("error: unable to read user input");
    trim_newline(&mut input);
    let pressure = input.parse::<f64>().expect(&format!("Given pressure ({}) was not a valid float!", input)[..]);
    println!("args were {}, {}", temperature, pressure);

    // Preparation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    const N: usize = 4;
    let a0 = 4.0248;
    let nbr_atoms = 4*N*N*N;
    let mut positions = vec![[0.0; 3]; nbr_atoms];
    let mut velocities = vec![[0.0; 3]; nbr_atoms];
    let mut accelerations = vec![[0.0; 3]; nbr_atoms];
    let mut forces = vec![[0.0; 3]; nbr_atoms];
    let root_area = BitMapBackend::new("images/excitationPerTimestep.png", (500, 400))
                    .into_drawing_area();
    root_area.fill(&WHITE).unwrap();

    let mut chart = ChartBuilder::on(&root_area)
        .set_label_area_size(LabelAreaPosition::Left, 60)
        .set_label_area_size(LabelAreaPosition::Bottom, 40)
        .caption("Energy per atom (AL)", ("Arial", 40))
        .build_ranged(0.0..0.5, 0.0..1.0)
        .unwrap();

    chart.configure_mesh()
        .x_desc("Time [picoseconds]")
        .y_desc("Energy [eV]")
        .draw().unwrap();

    // Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    const dt: f64 = 0.007;
    init_fcc(&mut positions, N, a0);
    velocities = vec![[0.0; 3]; nbr_atoms];
    accelerations = vec![[0.0; 3]; nbr_atoms];
    forces = vec![[0.0; 3]; nbr_atoms];    
    // Introduce small random deviations from the initial lattice positions. Uniform
    // deviations in the interval +- 6.5% of a0.
    perturb_lattice(&mut positions, a0);
    let initial_energy = get_energy_AL(&positions, a0*N as f64, nbr_atoms);

    let mut potential_energies: Vec<f64> = Vec::new();
    let mut kinetic_energies: Vec<f64> = Vec::new();

    let NO_OF_TIMESTEPS: usize = (2.0/dt) as usize; // Give the program two picoseconds to stabilize
    let mut times = vec![0.0; NO_OF_TIMESTEPS];
    for i in 0..NO_OF_TIMESTEPS{
        times[i] = i as f64 * dt;
    }

    for _ in 0..NO_OF_TIMESTEPS{
        velocity_verlet_timestep(&mut positions, &mut velocities, &mut accelerations, &mut forces,
                                a0, dt, N, nbr_atoms);
        let mut energy = 0.0;
        for v in velocities.iter(){
            energy += (v[0]*v[0] + v[1]*v[1] + v[2]*v[2]) / 2.0 * ALUMINIUM_MASS;
        }
        potential_energies.push( (get_energy_AL(&positions, a0*N as f64, nbr_atoms)
                                - initial_energy) / nbr_atoms as f64);
        kinetic_energies.push(energy / nbr_atoms as f64);
    }
}


// Thank you for the free boilerplate, stackexchange
fn trim_newline(s: &mut String) {
    if s.ends_with('\n') {
        s.pop();
        if s.ends_with('\r') {
            s.pop();
        }
    }
}